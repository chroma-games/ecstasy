// Reexports
pub use specs::{
    prelude::*,
    prelude,
    self,
    storage,
};

use std::ops::{Deref, DerefMut};
use specs::{
    prelude::*,
};

pub struct World {
    world: specs::World,
}

impl World {
    pub fn new() -> Self {
        Self {
            world: specs::World::new(),
        }
    }

    pub fn with_component<C: Component>(mut self) -> Self
        where C::Storage: Default
    {
        self.world.register::<C>();
        self
    }

    pub fn with_setup<G: WorldSetup>(mut self) -> Self {
        G::setup(&mut self);
        self
    }
}

impl Deref for World {
    type Target = specs::World;

    fn deref(&self) -> &Self::Target {
        &self.world
    }
}

impl DerefMut for World {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.world
    }
}

pub trait WorldSetup {
    fn setup(world: &mut World);
}
