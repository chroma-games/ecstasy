use ecstasy::{
    World,
    prelude::*,
    storage::VecStorage,
};

#[derive(Debug)]
struct Pos([f32; 3]);

impl Component for Pos {
    type Storage = VecStorage<Self>;
}

fn main() {
    let mut world = World::new()
        .with_component::<Pos>();

    let entity = world
        .create_entity()
        .with(Pos([1.0; 3]))
        .build();

    println!("{:?}", world.read_storage::<Pos>().get(entity));

    world.write_storage::<Pos>().remove(entity);

    println!("{:?}", world.read_storage::<Pos>().get(entity));
}
